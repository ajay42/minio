<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;

class FileController extends Controller
{
    public function store(Request $request)
    {
        Log::info($request);
        Log::info("yes");
        $file =$request->file('file');
//
//        // put file to local disk start
//        $path = public_path()."/images/Service/";
//        if (!File::exists($path)) {
//            File::makeDirectory($path, 0775, true);
//        }
//        $random = rand(111,999);
//        $imageReceived =explode("/",$file->getClientMimeType());
//        $imageName ="/images/Service/".time().$random.'.'.$imageReceived[1];
//        Log::info('Photo to be uploaded is  $imageName'.$imageName);
//        $file->move($path, $imageName);
        // put file to local disk end

        // put file on minio start
//        Automatically generate a unique ID for file name...
//        $res = Storage::disk('minio')->put('ok', $file);
//        Log::info($res);

//        Manually specify a file name...
        Storage::disk('minio')->putFileAs('ok', $file,'abc.jpg');
        // put file on minio end

        return back()->with('message', 'Your file is submitted Successfully');
    }

    public function download(){
//        1:- file name as url
//        2:- file name after download
        return Storage::cloud()->download('flooent-legal-web.tar.xz','flooent-legal-web.tar.xz');
    }
}
